import React from 'react'

// rafcp
const Login = () => {
    return (
        <div className='formContainer'>
            <div className='formWrapper'>
                <span className='logo'>Discount Discord Clone</span>
                <span className='title'>Login</span>
                
                <form>
                    <input type="email" placeholder='E-mail'/>
                    <input type="password" placeholder='Password'/> 

                    <button>Sign in</button>
                </form>

                <p>Don't have an account? Register</p>
            </div>
        </div>
    )
}

export default Login

