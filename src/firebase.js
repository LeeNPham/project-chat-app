import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";
import { getFirestore } from "firebase/firestore";
import { getStorage } from "firebase/storage";

const firebaseConfig = {
    apiKey: "AIzaSyAwwGP0J9ZABfJlWSOFNQniriCFbr7SGsc",
    authDomain: "discount-discord-41f2d.firebaseapp.com",
    projectId: "discount-discord-41f2d",
    storageBucket: "discount-discord-41f2d.appspot.com",
    messagingSenderId: "33167890992",
    appId: "1:33167890992:web:50462aae1bceabf9a6abf5"
};

// Initialize Firebase
export const app = initializeApp(firebaseConfig);
export const auth = getAuth()
export const storage = getStorage();
export const db = getFirestore();