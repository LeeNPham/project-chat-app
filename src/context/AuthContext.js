import { onAuthStateChanged } from "firebase/auth";
import { createContext , useState , useEffect } from "react";
import { auth } from "../firebase";

export const AuthContext = createContext();

export const AuthContextProvider = ({children}) =>{
    const [currentUser, setCurrentUser] = useState({})

    useEffect(()=>{
        onAuthStateChanged(auth, (user)=>{
            setCurrentUser(user);
            console.log(user)

        })
    },[]);

    <AuthContext.Provider value={{currentUser}}>
        {children}
    </AuthContext.Provider>
};

// look up contextAPI
// create an authentication provider
// create our user within the provider
// use that user variable within our App.js



