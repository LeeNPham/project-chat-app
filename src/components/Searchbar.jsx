import React from 'react'

const Searchbar = () => {
    return (
        <div className="searchbar">
            <div className="searchForm">
                <input type="text" placeholder='Search by Username'/>
            </div>

            <div className="userChat">
                <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ1Ltxo8pnCMx7uygFbr3V5cC-9SklWI3TlqtiWGteF_w&s" alt="" />
                <div className="userChatInfo">
                    <span>Steve Svirko</span>
                </div>
            </div>
        </div>
    )
}

export default Searchbar