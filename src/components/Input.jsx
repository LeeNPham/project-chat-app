import React from 'react'
import Img from '../img/img.png';
import Attach from '../img/attach.png';

const Input = () => {
    return (
        <div className='input'>
            <input type="text" placeholder='Type here...' />
            <div className="send">
                {/* <img src={Attach} alt="" /> */}

                <input type="file" style={{display: "none"}} id="file"/>
                <label htmlFor="file">
                    <img src={Attach} alt="" />
                </label>

                <input type="file" style={{display: "none"}} id="imgFile"/>
                <label htmlFor="imgFile">
                    <img src={Img} alt="" />
                </label>

                <button>Send</button>
            </div>
        </div>
    )
}

export default Input