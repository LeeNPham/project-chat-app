import React from 'react'

const Navbar = () => {
    return (
        <div className='navbar'>
            <span className="logo">Discount Discord Clone</span>
            <div className="user">
                <img src="https://i.pinimg.com/736x/9c/7d/8a/9c7d8a1a501d96d446cddb542b4b34b3.jpg" alt="" />
                <span>Bladeburner01</span>
                <button>logout</button>
            </div>
        </div>
    )
}

export default Navbar