import React from 'react'

const Message = () => {
    return (
        <div className='message owner'>
            <div className="messageInfo">
                <img src="https://i.pinimg.com/736x/9c/7d/8a/9c7d8a1a501d96d446cddb542b4b34b3.jpg" alt="" />
                <span>just now</span>
            </div>

            <div className="messageContent">
                <p>Let's make our own programming language with blackjack and hookers.</p>
                <img src="https://i.pinimg.com/736x/9c/7d/8a/9c7d8a1a501d96d446cddb542b4b34b3.jpg" alt="" />
            </div>
        </div>
    )
}

export default Message